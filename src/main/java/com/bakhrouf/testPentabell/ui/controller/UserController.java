package com.bakhrouf.testPentabell.ui.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bakhrouf.testPentabell.bll.entities.User;
import com.bakhrouf.testPentabell.dal.dao.UserRepository;
@RestController
@CrossOrigin("http://localhost:4200")
public class UserController {
	@Autowired
	private UserRepository userRepository;
	
	
	@PostMapping("/create")
	public User save(@RequestBody User u) {
		return userRepository.save(u);
	}
	 @GetMapping("/users")
	    public Iterable<User> getUsers() {
	        return userRepository.findAll();
	    }
	 @RequestMapping(value="/login/{login}/{password}",method=RequestMethod.GET)
	    public User login(@PathVariable String login,@PathVariable String password  ) {
		 if(!userRepository.findByLogin(login).isEmpty()) {
			 User user=(User) userRepository.findByLogin(login).get(0);
			 if(user.getPassword().equals(password))
				 return user;
		 		}
		 else 
			 return null;
		return null;

	    }

}
