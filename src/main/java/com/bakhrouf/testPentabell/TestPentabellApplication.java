package com.bakhrouf.testPentabell;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestPentabellApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestPentabellApplication.class, args);
	}

}
