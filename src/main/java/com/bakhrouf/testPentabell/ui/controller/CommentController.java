package com.bakhrouf.testPentabell.ui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bakhrouf.testPentabell.bll.entities.Comment;
import com.bakhrouf.testPentabell.bll.entities.Post;
import com.bakhrouf.testPentabell.bll.entities.User;
import com.bakhrouf.testPentabell.dal.dao.CommentRepository;
import com.bakhrouf.testPentabell.dal.dao.UserRepository;


@RestController
@CrossOrigin("http://localhost:4200")
public class CommentController {
	@Autowired
	private CommentRepository commentRepository;
	@Autowired
	private UserRepository userRepository;
	
	
	
	@PostMapping("/comment/{post}/{login}")
	public Comment save(@RequestBody Comment c,@PathVariable String post) {
		 User user=(User) userRepository.findByLogin(post).get(0);
		 c.setUser(user);		
		return this.commentRepository.save(c);
	}
}

