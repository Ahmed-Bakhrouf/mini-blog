package com.bakhrouf.testPentabell.dal.dao;

import org.springframework.data.repository.CrudRepository;

import com.bakhrouf.testPentabell.bll.entities.Post;

public interface PostRepository extends CrudRepository<Post, Long>{

}
