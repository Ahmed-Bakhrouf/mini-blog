package com.bakhrouf.testPentabell.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
@EnableWebSecurity
public class CustomWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	 http .csrf() .disable() .cors() .and().authorizeRequests().antMatchers("/").permitAll().antMatchers(HttpMethod.POST, "/create").
    	 permitAll().antMatchers(HttpMethod.GET, "/login/*/*").anonymous()
    	 .antMatchers(HttpMethod.POST,"/post/*").permitAll().antMatchers(HttpMethod.GET,"/getPosts*").permitAll();
    }

	

	
	  @Autowired public void configureGlobal(AuthenticationManagerBuilder
	  authenticationMgr) throws Exception {
	  authenticationMgr.inMemoryAuthentication().withUser("employee").password(
	  "employee")
	  .authorities("ROLE_USER").and().withUser("javainuse").password("javainuse")
	  .authorities("ROLE_USER", "ROLE_ADMIN"); }
	 

}