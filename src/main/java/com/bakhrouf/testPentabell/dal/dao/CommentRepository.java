package com.bakhrouf.testPentabell.dal.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bakhrouf.testPentabell.bll.entities.Comment;

public interface CommentRepository extends JpaRepository<Comment,Long>{

}
