package com.bakhrouf.testPentabell.dal.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.bakhrouf.testPentabell.bll.entities.User;

public interface UserRepository extends CrudRepository<User, Long>{
	List<User> findByLogin(String login);

}
