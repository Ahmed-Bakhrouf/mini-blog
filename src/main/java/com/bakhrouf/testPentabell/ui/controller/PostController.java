package com.bakhrouf.testPentabell.ui.controller;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bakhrouf.testPentabell.bll.entities.Post;
import com.bakhrouf.testPentabell.bll.entities.User;
import com.bakhrouf.testPentabell.dal.dao.PostRepository;
import com.bakhrouf.testPentabell.dal.dao.UserRepository;
@RestController
@CrossOrigin("http://localhost:4200")
public class PostController {
	@Autowired
	private PostRepository postRepository;
	@Autowired
	private UserRepository userRepository;
	
	
	
	@PostMapping("/post/{login}")
	public Post save(@RequestBody Post p,@PathVariable String login) {
		 User user=(User) userRepository.findByLogin(login).get(0);
		 p.setUser(user);		
		return postRepository.save(p);
	}
	@GetMapping("/getPosts")
	public Iterable<Post> get() {
		return postRepository.findAll();
	}
}
